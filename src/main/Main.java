/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import UI.AppMenu;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import structs.Locatie;
import structs.Oras;
import structs.PerioadaVacanta;

/**
 *
 * @author bala
 */
public class Main
{   
    public static AppMenu meniu_aplicatie;
    public static File input_data;
    public static String linie;
    public static String[] date;
    public static Scanner scan;
    public static HashMap<String,Locatie> date_locatii;
    public static HashMap<String,ArrayList<Locatie>> locatii_tari;
    public static HashMap<String,ArrayList<Locatie>> locatii_judete;
    public static HashMap<String,ArrayList<Locatie>> locatii_orase;
    public static HashMap<String,ArrayList<Locatie>> locatii_activitati;
    
    
    public static void main(String[] _args)
    {
       input_data=new File("/home/bala/Desktop/date.txt");
       date_locatii=new HashMap<>();
       locatii_tari=new HashMap<>();
       locatii_judete=new HashMap<>();
       locatii_orase=new HashMap<>();
       locatii_activitati=new HashMap<>();
       try
       {
            scan=new Scanner(input_data);
            readData(scan);
            meniu_aplicatie=new AppMenu("Aplicatie Backend");
       }
       catch (FileNotFoundException ex1)
       {
           System.out.println("FileNotFoundException: "+ex1.getMessage());
       }
    }
    
    
    public static void readData(Scanner scan)
    {
            while(scan.hasNextLine())
            {
                int i;
                linie=scan.nextLine();
                if (linie.equals(""))
                    continue;
                date=linie.split(",");
                String nume_locatie;
                String nume_oras;
                String nume_judet;
                String nume_tara;
                String moneda_nationala;
                float pret_mediu;
                String perioada_de_vacanta;
                ArrayList<String> activitati;
                activitati=new ArrayList<>();
                
                    nume_locatie=date[0];
                    System.out.println(nume_locatie);
                    nume_oras=date[1];
                    nume_judet=date[2];
                    nume_tara=date[3];
                    moneda_nationala=date[4];
                    pret_mediu=Float.parseFloat(date[5]);
                    perioada_de_vacanta=date[6];
                    for (i=7;i<date.length;i++)
                        activitati.add(date[i]);
                Oras oras=new Oras(nume_oras,nume_judet,nume_tara,moneda_nationala);
                PerioadaVacanta perioada_vacanta=new PerioadaVacanta(perioada_de_vacanta);
                Locatie locatie=new Locatie(nume_locatie,oras,pret_mediu,perioada_vacanta,activitati);
                date_locatii.put(nume_locatie,locatie);//am adaugat toate locatiile in hashmap,key-ul fiind numele
                
                
                
                
                //pun toate locatiile dintr-o anumita tara intr-un hashmap ca sa pot extrage imediat toate locatiile in functie de tara
                if (locatii_tari.containsKey(locatie.getNumeTara())==false)//daca nu exista nicio locatie din acea tara in hashmap creez un ArrayList nou,adaug in ArrayList locatia curenta si apoi mapez ArrayList-ul la numele tarii
                {
                    ArrayList<Locatie> locatii=new ArrayList<>();
                    locatii.add(locatie);
                    locatii_tari.put(locatie.getNumeTara(),locatii);
                }
                else
                {
                    locatii_tari.get(locatie.getNumeTara()).add(locatie);//daca deja exista ArrayList-ul de locatii pt acea tara introduc in ArrayList noua locatie
                }
                
                
                
                //pun toate locatiile dintr-un  anumit judet intr-un hashmap ca sa pot extrage imediat toate locatiile in functie de judet
                if (locatii_judete.containsKey(locatie.getNumeJudet())==false)//daca nu exista nicio locatie din acel judet in hashmap creez un ArrayList nou,adaug in ArrayList locatia curenta si apoi mapez ArrayList-ul la numele judetului  
                {
                    ArrayList<Locatie> locatii=new ArrayList<>();
                    locatii.add(locatie);
                    locatii_judete.put(locatie.getNumeJudet(),locatii);
                }
                else
                {
                    locatii_judete.get(locatie.getNumeJudet()).add(locatie);//daca deja exista ArrayList-ul de locatii pt acel judet introduc in ArrayList noua locatie
                }
                
                
                
                //pun toate locatiile dintr-un  anumit oras intr-un hashmap ca sa pot extrage imediat toate locatiile in functie de oras
                if (locatii_orase.containsKey(locatie.getNumeOras())==false)//daca nu exista nicio locatie din acel oras in hashmap creez un ArrayList nou,adaug in ArrayList locatia curenta si apoi mapez ArrayList-ul la numele orasului  
                {
                    ArrayList<Locatie> locatii=new ArrayList<>();
                    locatii.add(locatie);
                    locatii_orase.put(locatie.getNumeOras(),locatii);
                }
                else
                {
                    locatii_orase.get(locatie.getNumeOras()).add(locatie);//daca deja exista ArrayList-ul de locatii pt acel oras introduc in ArrayList noua locatie
                }
                
                
                
                //pun toate activitatile care se pot practica in locatia respectiva in hashmap
                for (i=0;i<activitati.size();i++)
                {
                    if (locatii_activitati.containsKey(activitati.get(i))==false)//daca nu exista activitatea in hashmap creez un ArrayList de locatii,introduc locatia curenta in ArrayList si mapez ArrayList-ul la numele activitatii
                    {
                        ArrayList<Locatie> locatii=new ArrayList<>();
                        locatii.add(locatie);
                        locatii_activitati.put(activitati.get(i), locatii);
                    }
                    else//daca deja e mapat un ArrayList la activitatea respectia doar adaug o locatie noua unde se poate practica acea activitate in hashmap
                    {
                        locatii_activitati.get(activitati.get(i)).add(locatie);
                    }
                }
                
            }
    }
       
}
    
    
