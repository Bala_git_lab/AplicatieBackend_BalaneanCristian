/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package structs;

/**
 *
 * @author bala
 */
public class Oras extends Judet
{
    private final String nume_oras;
    public Oras(String nume_oras,String nume_judet,String nume_tara,String moneda_nationala)
    {
        super (nume_judet,nume_tara,moneda_nationala);
        this.nume_oras=nume_oras;
    }
    public String getNumeOras()
    {
        return nume_oras;
    }
}
