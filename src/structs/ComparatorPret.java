/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package structs;

import java.util.Comparator;

/**
 *
 * @author bala
 */
public class ComparatorPret implements Comparator<Locatie>
{

    @Override
    public int compare(Locatie a, Locatie b) 
    {
        if (a.cost_total>b.cost_total)
            return 1;
        if (a.cost_total<b.cost_total)
            return -1;
        return 0;
    }
    
}
