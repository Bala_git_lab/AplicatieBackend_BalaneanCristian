/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package structs;

/**
 *
 * @author bala
 */
public class Judet extends Tara 
{
    private final String nume_judet;
    public Judet(String nume_judet,String nume_tara,String moneda_nationala)
    {
        super(nume_tara,moneda_nationala);
        this.nume_judet=nume_judet;
    }
    public String getNumeJudet()
    {
        return nume_judet;
    }
}
