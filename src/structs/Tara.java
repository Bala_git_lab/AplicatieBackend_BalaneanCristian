/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package structs;

/**
 *
 * @author bala
 */
public class Tara
{
    private final String nume_tara;
    private final String moneda_nationala;
    public Tara(String nume_tara,String moneda_nationala)
    {
        this.nume_tara=nume_tara;
        this.moneda_nationala=moneda_nationala;
    }
    public String getNumeTara()
    {
        return nume_tara;
    }
    public String getMonedaNationala()
    {
        return moneda_nationala;
    }
}
