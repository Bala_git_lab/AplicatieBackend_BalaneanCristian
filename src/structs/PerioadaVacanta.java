/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package structs;

/**
 *
 * @author bala
 */
public class PerioadaVacanta 
{
    public final int zi_inceput;
    public final int luna_inceput;
    public final int an_inceput;
    public final int zi_final;
    public final int luna_final;
    public final int an_final;
    public final String perioada;
    public final int nr_zile;
    public PerioadaVacanta(String perioada)
    {
        this.perioada=perioada;
        String[] date;
        date=perioada.split("[.-]");
        this.zi_inceput=Integer.parseInt(date[0]);
        this.luna_inceput=Integer.parseInt(date[1]);
        this.an_inceput=Integer.parseInt(date[2]);
        this.zi_final=Integer.parseInt(date[3]);
        this.luna_final=Integer.parseInt(date[4]);
        this.an_final=Integer.parseInt(date[5]);
        this.nr_zile=(int)((this.an_final-this.an_inceput)*365+((this.luna_final-this.luna_inceput)*365)/12+(this.zi_final-this.zi_inceput));
    }
    public String getPerioada()
    {
        return perioada;
    }
    public int getNrZile()
    {
        return this.nr_zile;
    }
}
