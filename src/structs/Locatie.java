/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package structs;

import java.util.ArrayList;

/**
 *
 * @author bala
 */
public class Locatie
{
    public final String nume_locatie;
    private final Oras oras;
    private final float pret_mediu_pe_zi;
    private final PerioadaVacanta perioada_vacanta;
    public float cost_total;
    private final ArrayList<String> activitati;
    public Locatie(String nume_locatie,Oras oras,float pret_mediu_pe_zi,PerioadaVacanta perioada_vacanta,ArrayList<String> activitati)
    {
        this.nume_locatie=nume_locatie;
        this.oras=oras;
        this.pret_mediu_pe_zi=pret_mediu_pe_zi;
        this.perioada_vacanta=perioada_vacanta;
        this.activitati=activitati;
    }
    public String getNumeOras()
    {
        return oras.getNumeOras();
    }
    public String getNumeJudet()
    {
        return oras.getNumeJudet();
    }
    public String getNumeTara()
    {
        return oras.getNumeTara();
    }
    public String getMonedaNationala()
    {
        return oras.getMonedaNationala();
    }
    public Oras getOras()
    {
        return oras;
    }
    public float getPretMediuPeZi()
    {
        return pret_mediu_pe_zi;
    }
    public String getPerioadaVacantaString()
    {
        return perioada_vacanta.perioada;
    }
    public PerioadaVacanta getPerioadaVacanta()
    {
        return perioada_vacanta;
    }
    public ArrayList<String> getActivitati()
    {
        return activitati;
    }
    public float getCostTotal()
    {
        return cost_total;
    }
}
