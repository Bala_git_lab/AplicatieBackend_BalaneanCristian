/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 *
 * @author bala
 */
public class LocationData extends JFrame implements ActionListener
{
    public JPanel date_locatie_panel;
    public JTextArea date_locatie_text_area;
    public double screen_width;
    public double screen_height;
    public LocationData(String titlu)
    {
        super(titlu);
        initComponents();
        pack();
        setVisible(true);
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
    public void initComponents()
    {
        date_locatie_panel = new JPanel(new GridBagLayout());
        GridBagConstraints c;
        c = new GridBagConstraints();
        c.weightx=0.5;
        c.weighty=0.5;
         
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        screen_width = screenSize.getWidth();
        screen_height = screenSize.getHeight();
        this.setMinimumSize(new Dimension((int)(this.screen_width/3),(int)(this.screen_height/3)));
        
        
        date_locatie_text_area=new JTextArea(20,50);
        c.gridx=0;
        c.gridy=0;
        c.gridwidth=3;
        c.gridheight=3;
        date_locatie_panel.add(date_locatie_text_area,c);
        
        
        add(date_locatie_panel);
    }
    @Override
    public void actionPerformed(ActionEvent ae) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
