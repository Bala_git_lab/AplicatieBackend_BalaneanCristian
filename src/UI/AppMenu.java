/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import main.Main;
import structs.ComparatorPret;
import structs.Locatie;
import structs.PerioadaVacanta;

/**
 *
 * @author bala
 */
public class AppMenu extends JFrame implements ActionListener
{
    public JPanel meniuPanel;
    public JLabel titlu_aplicatie;
    public Font meniu_font;
    
    public JLabel location_data_text;
    public JTextField location_data_text_field;
    public JButton location_data_button;
    public JLabel feedback_location_data_text;
    
    public JLabel top_five_text;
    public JRadioButton top_five_tara_radio_button;
    public JRadioButton top_five_judet_radio_button;
    public JRadioButton top_five_oras_radio_button;
    public JLabel begin_date_text;
    public JLabel end_date_text;
    public JTextField top_five_day_begin;
    public JTextField top_five_month_begin;
    public JTextField top_five_year_begin;
    public JTextField top_five_day_end;
    public JTextField top_five_month_end;
    public JTextField top_five_year_end;
    public JButton top_five_button;
    public JLabel top_five_select_name_label;
    public JTextField top_five_select_name_text_field;
    public JLabel top_five_feedback_label;
    
    
    
    public JLabel activitate_ieftina_label;
    public JTextField activitate_ieftina_text_field;
    public JButton activitate_ieftina_button;
    public JLabel activitate_ieftina_feedback_label;
    
    
            
    public double screen_width;
    public double screen_height;
    Locatie locatie_optima;
    public ArrayList<Locatie> locatii_tara_sortate;
    public ArrayList<Locatie> locatii_judet_sortate;
    public ArrayList<Locatie> locatii_oras_sortate;
    public ArrayList<Locatie> locatii_activitati_realizabile;
    public AppMenu(String titlu)
    {
        super(titlu);
        initComponents();
        pack();
        setVisible(true);
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    public void initComponents()
    {
        meniuPanel = new JPanel(new GridBagLayout());
        GridBagConstraints c;
        c = new GridBagConstraints();
        c.weightx=0.5;
        c.weighty=0.5;
         
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        screen_width = screenSize.getWidth();
        screen_height = screenSize.getHeight();
        meniuPanel.setBackground(new Color(0,128,255));
        this.setMinimumSize(new Dimension((int)(this.screen_width/2),(int)(this.screen_height/1.5)));
        
        
        titlu_aplicatie=new JLabel();
        titlu_aplicatie.setText("Ghid pentru vacanta");
        meniu_font=new Font("Gill Sans Ultra Bold",Font.BOLD,30);
        titlu_aplicatie.setVerticalAlignment(JLabel.CENTER);
        titlu_aplicatie.setHorizontalAlignment(JLabel.CENTER);
        titlu_aplicatie.setFont(meniu_font);
        titlu_aplicatie.setForeground(new Color(255,94,0));
        c.gridx=0;
        c.gridy=0;
        c.gridwidth=10;
        c.gridheight=1;
        meniuPanel.add(titlu_aplicatie, c);
        
        
        location_data_text=new JLabel();
        location_data_text.setText("Cauta date despre o locatie:");
        meniu_font=new Font("Gill Sans Ultra Bold",Font.BOLD,15);
        location_data_text.setFont(meniu_font);
        location_data_text.setForeground(Color.BLACK);
        c.gridx=0;
        c.gridy=1;
        c.gridwidth=3;
        c.gridheight=1;
        c.anchor=GridBagConstraints.SOUTHWEST;
        meniuPanel.add(location_data_text, c);
        
        
        location_data_text_field=new JTextField(14);
        c.gridx=0;
        c.gridy=2;
        c.gridwidth=3;
        c.gridheight=1;
        c.anchor=GridBagConstraints.WEST;
        meniuPanel.add(location_data_text_field, c);
        
        
        location_data_button=new JButton();
        location_data_button.setText("cauta");
        c.gridx=0;
        c.gridy=3;
        c.gridwidth=2;
        c.gridheight=1;
        c.anchor=GridBagConstraints.NORTHWEST;
        meniuPanel.add(location_data_button, c);
        
        
        feedback_location_data_text=new JLabel();
        feedback_location_data_text.setText("");
        meniu_font=new Font("Gill Sans Ultra Bold",Font.BOLD,10);
        feedback_location_data_text.setFont(meniu_font);
        feedback_location_data_text.setForeground(Color.BLACK);
        c.gridx=0;
        c.gridy=4;
        c.gridwidth=3;
        c.gridheight=1;
        c.anchor=GridBagConstraints.NORTHWEST;
        meniuPanel.add(feedback_location_data_text, c);
        
        
        
        
        
        activitate_ieftina_label=new JLabel();
        activitate_ieftina_label.setText("Activitate ieftina:");
        meniu_font=new Font("Gill Sans Ultra Bold",Font.BOLD,15);
        activitate_ieftina_label.setFont(meniu_font);
        activitate_ieftina_label.setForeground(Color.BLACK);
        c.gridx=0;
        c.gridy=5;
        c.gridwidth=3;
        c.gridheight=1;
        c.anchor=GridBagConstraints.NORTHWEST;
        meniuPanel.add(activitate_ieftina_label, c);
        
        
        activitate_ieftina_text_field=new JTextField(16);
        c.gridx=0;
        c.gridy=6;
        c.gridwidth=3;
        c.gridheight=1;
        c.anchor=GridBagConstraints.NORTHWEST;
        meniuPanel.add(activitate_ieftina_text_field, c);
        
        
        activitate_ieftina_button=new JButton();
        activitate_ieftina_button.setText("cauta locatia optima");
        c.gridx=0;
        c.gridy=7;
        c.gridwidth=3;
        c.gridheight=1;
        c.anchor=GridBagConstraints.NORTHWEST;
        meniuPanel.add(activitate_ieftina_button, c);
        
        
        activitate_ieftina_feedback_label=new JLabel();
        activitate_ieftina_feedback_label.setText("");
        meniu_font=new Font("Gill Sans Ultra Bold",Font.BOLD,10);
        activitate_ieftina_feedback_label.setFont(meniu_font);
        activitate_ieftina_feedback_label.setForeground(Color.BLACK);
        c.gridx=0;
        c.gridy=8;
        c.gridwidth=3;
        c.gridheight=1;
        c.anchor=GridBagConstraints.NORTHWEST;
        meniuPanel.add(activitate_ieftina_feedback_label, c);
        
        
        
        
        
        
        top_five_text=new JLabel();
        top_five_text.setText("Cauta top 5 locatii:");
        meniu_font=new Font("Gill Sans Ultra Bold",Font.BOLD,15);
        top_five_text.setFont(meniu_font);
        top_five_text.setForeground(Color.BLACK);
        c.gridx=2;
        c.gridy=1;
        c.gridwidth=3;
        c.gridheight=1;
        c.anchor=GridBagConstraints.SOUTH;
        meniuPanel.add(top_five_text, c);
        
        
        top_five_tara_radio_button=new JRadioButton();
        top_five_tara_radio_button.setText("Tara");
        top_five_tara_radio_button.setBackground(new Color(0,128,255));
        c.gridx=2;
        c.gridy=2;
        c.gridwidth=1;
        c.gridheight=1;
        c.anchor=GridBagConstraints.EAST;
        top_five_tara_radio_button.setSelected(true);
        meniuPanel.add(top_five_tara_radio_button, c);
        
        
        top_five_judet_radio_button=new JRadioButton();
        top_five_judet_radio_button.setText("Judet");
        top_five_judet_radio_button.setBackground(new Color(0,128,255));
        c.gridx=3;
        c.gridy=2;
        c.gridwidth=1;
        c.gridheight=1;
        c.anchor=GridBagConstraints.CENTER;
        meniuPanel.add(top_five_judet_radio_button, c);
        
        
        top_five_oras_radio_button=new JRadioButton();
        top_five_oras_radio_button.setText("Oras");
        top_five_oras_radio_button.setBackground(new Color(0,128,255));
        c.gridx=4;
        c.gridy=2;
        c.gridwidth=1;
        c.gridheight=1;
        c.anchor=GridBagConstraints.WEST;
        meniuPanel.add(top_five_oras_radio_button, c);
        
        
        top_five_select_name_label=new JLabel();
        top_five_select_name_label.setText("Nume:");
        meniu_font=new Font("Gill Sans Ultra Bold",Font.BOLD,15);
        top_five_select_name_label.setFont(meniu_font);
        top_five_select_name_label.setForeground(Color.BLACK);
        c.gridx=2;
        c.gridy=3;
        c.gridwidth=1;
        c.gridheight=1;
        c.anchor=GridBagConstraints.EAST;
        meniuPanel.add(top_five_select_name_label, c);
        
        
        top_five_select_name_text_field=new JTextField(18);
        c.gridx=3;
        c.gridy=3;
        c.gridwidth=3;
        c.gridheight=1;
        c.anchor=GridBagConstraints.CENTER;
        meniuPanel.add(top_five_select_name_text_field, c);
        
        
        begin_date_text=new JLabel();
        begin_date_text.setText("Data initiala:");
        meniu_font=new Font("Gill Sans Ultra Bold",Font.BOLD,15);
        begin_date_text.setFont(meniu_font);
        begin_date_text.setForeground(Color.BLACK);
        c.gridx=2;
        c.gridy=4;
        c.gridwidth=1;
        c.gridheight=1;
        c.anchor=GridBagConstraints.EAST;
        meniuPanel.add(begin_date_text, c);
        
        
        top_five_day_begin=new JTextField(2);
        c.gridx=3;
        c.gridy=4;
        c.gridwidth=1;
        c.gridheight=1;
        c.anchor=GridBagConstraints.EAST;
        meniuPanel.add(top_five_day_begin, c);
        
        
        top_five_month_begin=new JTextField(2);
        c.gridx=4;
        c.gridy=4;
        c.gridwidth=1;
        c.gridheight=1;
        c.anchor=GridBagConstraints.CENTER;
        meniuPanel.add(top_five_month_begin, c);
        
        
        top_five_year_begin=new JTextField(4);
        c.gridx=5;
        c.gridy=4;
        c.gridwidth=1;
        c.gridheight=1;
        c.anchor=GridBagConstraints.WEST;
        meniuPanel.add(top_five_year_begin, c);
        
        
        end_date_text=new JLabel();
        end_date_text.setText("Data finala:");
        meniu_font=new Font("Gill Sans Ultra Bold",Font.BOLD,15);
        end_date_text.setFont(meniu_font);
        end_date_text.setForeground(Color.BLACK);
        c.gridx=2;
        c.gridy=5;
        c.gridwidth=1;
        c.gridheight=1;
        c.anchor=GridBagConstraints.EAST;
        meniuPanel.add(end_date_text, c);
        
        
        top_five_day_end=new JTextField(2);
        c.gridx=3;
        c.gridy=5;
        c.gridwidth=1;
        c.gridheight=1;
        c.anchor=GridBagConstraints.EAST;
        meniuPanel.add(top_five_day_end, c);
        
        
        top_five_month_end=new JTextField(2);
        c.gridx=4;
        c.gridy=5;
        c.gridwidth=1;
        c.gridheight=1;
        c.anchor=GridBagConstraints.CENTER;
        meniuPanel.add(top_five_month_end, c);
        
        
        top_five_year_end=new JTextField(4);
        c.gridx=5;
        c.gridy=5;
        c.gridwidth=1;
        c.gridheight=1;
        c.anchor=GridBagConstraints.WEST;
        meniuPanel.add(top_five_year_end, c);
        
        
        top_five_button=new JButton();
        top_five_button.setText("Top 5 locatii");
        c.gridx=2;
        c.gridy=6;
        c.gridwidth=3;
        c.gridheight=1;
        c.anchor=GridBagConstraints.CENTER;
        meniuPanel.add(top_five_button, c);
        
        
        top_five_feedback_label=new JLabel();
        top_five_feedback_label.setText("");
        meniu_font=new Font("Gill Sans Ultra Bold",Font.BOLD,12);
        top_five_feedback_label.setFont(meniu_font);
        top_five_feedback_label.setForeground(Color.BLACK);
        c.gridx=2;
        c.gridy=7;
        c.gridwidth=3;
        c.gridheight=1;
        c.anchor=GridBagConstraints.NORTH;
        meniuPanel.add(top_five_feedback_label, c);
        
        
        
        
        location_data_button.addActionListener(this);
        location_data_text_field.addActionListener(this);
        top_five_tara_radio_button.addActionListener(this);
        top_five_judet_radio_button.addActionListener(this);
        top_five_oras_radio_button.addActionListener(this);
        top_five_button.addActionListener(this);
        activitate_ieftina_button.addActionListener(this);
        activitate_ieftina_text_field.addActionListener(this);
        
        
         add(meniuPanel);
    }
    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource()==location_data_button||ae.getSource()==location_data_text_field)
        {
            String nume_locatie;
            nume_locatie=location_data_text_field.getText();
            location_data_text_field.setText("");
            if (Main.date_locatii.isEmpty())
            {
                feedback_location_data_text.setText("Nu exista locatii in baza de date");
            }
            else
            {
                if (nume_locatie.equals("")==false)
                {
                    if (Main.date_locatii.containsKey(nume_locatie))
                    {
                        feedback_location_data_text.setText("Am gasit date despre locatie");
                        displayLocationInfo(nume_locatie);
                  
                    
                        System.out.println("Locatii din "+Main.date_locatii.get(nume_locatie).getNumeTara()+": ");
                        for (int i=0;i<Main.locatii_tari.get(Main.date_locatii.get(nume_locatie).getNumeTara()).size();i++)
                            System.out.println(Main.locatii_tari.get(Main.date_locatii.get(nume_locatie).getNumeTara()).get(i).nume_locatie);
                    }
                    else
                    {
                        feedback_location_data_text.setText("Nu exista locatia in baza de date");
                    }
                }
                else
                {
                    feedback_location_data_text.setText("Campul este gol");
                }
            }
        }
        if (ae.getSource()==top_five_tara_radio_button)
        {
            top_five_tara_radio_button.setSelected(true);
            top_five_judet_radio_button.setSelected(false);
            top_five_oras_radio_button.setSelected(false);
        }
        if (ae.getSource()==top_five_judet_radio_button)
        {
            top_five_judet_radio_button.setSelected(true);
            top_five_tara_radio_button.setSelected(false);
            top_five_oras_radio_button.setSelected(false);
        }
        if (ae.getSource()==top_five_oras_radio_button)
        {
            top_five_oras_radio_button.setSelected(true);
            top_five_judet_radio_button.setSelected(false);
            top_five_tara_radio_button.setSelected(false);
        }
        if(ae.getSource()==top_five_button)
        {
            
            
            
            if (top_five_tara_radio_button.isSelected()==true)//am selectat tara
            {
                
                String tara;
                int zi_start;
                int luna_start;
                int an_start;
                int zi_final;
                int luna_final;
                int an_final;
                boolean valid_data=false;
                
                try
                {   
                    //extragerea datelor din interfata grafica
                    tara=this.top_five_select_name_text_field.getText();
                    zi_start=Integer.parseInt(this.top_five_day_begin.getText());
                    luna_start=Integer.parseInt(this.top_five_month_begin.getText());
                    an_start=Integer.parseInt(this.top_five_year_begin.getText());
                    zi_final=Integer.parseInt(this.top_five_day_end.getText());
                    luna_final=Integer.parseInt(this.top_five_month_end.getText());
                    an_final=Integer.parseInt(this.top_five_year_end.getText());
                    
                    //verificarea datelor de intrare
                    if(tara.equals(""))
                        top_five_feedback_label.setText("Campul cu numele tarii este gol");
                    else
                    {
                        //verific daca zilele sunt in intervalul [1-31] daca lunile sunt in intervalul [1-12] si daca anii sunt pozitivi(normal ar fi sa fie mai mari sau egali cu 2018)
                        if(zi_start>0&&zi_start<=31&&zi_final>0&&zi_final<=31&&luna_start>0&&luna_start<=12&&luna_final>0&&luna_final<=12&&an_start>0&&an_final>0)
                            valid_data=true;
                        //verific daca nr de zile e mai mare ca 0 
                        int nr_zile=(int)((an_final-an_start)*365+((luna_final-luna_start)*365)/12+(zi_final-zi_start));
                        if (nr_zile<=0)
                            valid_data=false;
                        
                        
                        
                        if(valid_data)
                        {
                            displayTopFiveInTara(tara,zi_start,luna_start,an_start,zi_final,luna_final,an_final);
                        }
                        else
                        {
                            top_five_feedback_label.setText("Date invalide");
                        }
                    }
                }
                catch (NumberFormatException ex)
                {
                    top_five_feedback_label.setText("Date invalide");
                }
            }
            
            
            
            if (top_five_judet_radio_button.isSelected()==true)//am selectat judetul
            {
                String judet;
                int zi_start;
                int luna_start;
                int an_start;
                int zi_final;
                int luna_final;
                int an_final;
                boolean valid_data=false;
                
                try
                {
                    //extragerea datelor din interfata grafica
                    judet=this.top_five_select_name_text_field.getText();
                    zi_start=Integer.parseInt(this.top_five_day_begin.getText());
                    luna_start=Integer.parseInt(this.top_five_month_begin.getText());
                    an_start=Integer.parseInt(this.top_five_year_begin.getText());
                    zi_final=Integer.parseInt(this.top_five_day_end.getText());
                    luna_final=Integer.parseInt(this.top_five_month_end.getText());
                    an_final=Integer.parseInt(this.top_five_year_end.getText());
                    
                    //verificarea datelor de intrare
                    if(judet.equals(""))
                        top_five_feedback_label.setText("Campul cu numele judetului este gol");
                    else
                    {
                        //verific daca zilele sunt in intervalul [1-31] daca lunile sunt in intervalul [1-12] si daca anii sunt pozitivi(normal ar fi sa fie mai mari sau egali cu 2018)
                        if(zi_start>0&&zi_start<=31&&zi_final>0&&zi_final<=31&&luna_start>0&&luna_start<=12&&luna_final>0&&luna_final<=12&&an_start>0&&an_final>0)
                            valid_data=true;
                        //verific daca nr de zile e mai mare ca 0 
                        int nr_zile=(int)((an_final-an_start)*365+((luna_final-luna_start)*365)/12+(zi_final-zi_start));
                        if (nr_zile<=0)
                            valid_data=false;
                        
                        
                        
                        if(valid_data)
                        {
                            displayTopFiveInJudet(judet,zi_start,luna_start,an_start,zi_final,luna_final,an_final);
                        }
                        else
                        {
                            top_five_feedback_label.setText("Date invalide");
                        }
                    }
                }
                catch(NumberFormatException ex)
                {
                    top_five_feedback_label.setText("Date invalide");
                }
               
            }
            
            
            
            if (top_five_oras_radio_button.isSelected()==true)//am selectat orasul
            {
                String oras;
                int zi_start;
                int luna_start;
                int an_start;
                int zi_final;
                int luna_final;
                int an_final;
                boolean valid_data=false;
                try
                {
                    //extragerea datelor din interfata grafica
                    oras=this.top_five_select_name_text_field.getText();
                    zi_start=Integer.parseInt(this.top_five_day_begin.getText());
                    luna_start=Integer.parseInt(this.top_five_month_begin.getText());
                    an_start=Integer.parseInt(this.top_five_year_begin.getText());
                    zi_final=Integer.parseInt(this.top_five_day_end.getText());
                    luna_final=Integer.parseInt(this.top_five_month_end.getText());
                    an_final=Integer.parseInt(this.top_five_year_end.getText());
                    
                    //verificarea datelor de intrare
                    if(oras.equals(""))
                        top_five_feedback_label.setText("Campul cu numele orasului este gol");
                    else
                    {
                        //verific daca zilele sunt in intervalul [1-31] daca lunile sunt in intervalul [1-12] si daca anii sunt pozitivi(normal ar fi sa fie mai mari sau egali cu 2018)
                        if(zi_start>0&&zi_start<=31&&zi_final>0&&zi_final<=31&&luna_start>0&&luna_start<=12&&luna_final>0&&luna_final<=12&&an_start>0&&an_final>0)
                            valid_data=true;
                        //verific daca nr de zile e mai mare ca 0 
                        int nr_zile=(int)((an_final-an_start)*365+((luna_final-luna_start)*365)/12+(zi_final-zi_start));
                        if (nr_zile<=0)
                            valid_data=false;
                        
                        
                        if(valid_data)
                        {
                            displayTopFiveInOras(oras,zi_start,luna_start,an_start,zi_final,luna_final,an_final);
                        }
                        else
                        {
                            top_five_feedback_label.setText("Date invalide");
                        }
                    }
                }
                catch(NumberFormatException ex)
                {
                    top_five_feedback_label.setText("Date invalide");
                }
            }
        }
        
        
        
        
        
        if (ae.getSource()==activitate_ieftina_button||ae.getSource()==activitate_ieftina_text_field)
        {
            String activitate;
            activitate=activitate_ieftina_text_field.getText();
            if (activitate.equals(""))
            {
                activitate_ieftina_feedback_label.setText("Campul este gol");
            }
            else
            {
                DisplayActivitateIeftina(activitate);
            }
        }
        
        
    }
    public void displayLocationInfo(String nume_locatie)
    {
        LocationData location_info=new LocationData(nume_locatie);
                    String informatii;
                    informatii="Nume locatie: "+nume_locatie+"\n";
                    informatii=informatii+"Oras: "+Main.date_locatii.get(nume_locatie).getNumeOras()+"\n";
                    informatii=informatii+"Judet: "+Main.date_locatii.get(nume_locatie).getNumeJudet()+"\n";
                    informatii=informatii+"Tara: "+Main.date_locatii.get(nume_locatie).getNumeTara()+"\n";
                    float pret=Main.date_locatii.get(nume_locatie).getPretMediuPeZi();
                    String pret_string=Float.toString(pret);
                    informatii=informatii+"Pret mediu pe zi: "+pret_string+" "+Main.date_locatii.get(nume_locatie).getMonedaNationala()+"\n";
                    informatii=informatii+"Perioada disponibila: "+Main.date_locatii.get(nume_locatie).getPerioadaVacantaString()+"\n";
                    informatii=informatii+"Activitati: ";
                    for (int i=0;i<Main.date_locatii.get(nume_locatie).getActivitati().size()-1;i++)
                         informatii=informatii+Main.date_locatii.get(nume_locatie).getActivitati().get(i)+", ";
                    informatii=informatii+Main.date_locatii.get(nume_locatie).getActivitati().get(Main.date_locatii.get(nume_locatie).getActivitati().size()-1);
                    location_info.date_locatie_text_area.setText(informatii);
    }
    public void displayTopFiveInTara(String tara,int zi_start,int luna_start,int an_start,int zi_final,int luna_final,int an_final)
    {
        locatii_tara_sortate=new ArrayList<>();
        ArrayList<Locatie> locatii_tara;
        if (Main.locatii_tari.containsKey(tara))
        {
            locatii_tara=Main.locatii_tari.get(tara);
            for (int i=0;i<locatii_tara.size();i++)
            {
                PerioadaVacanta perioada_vacanta;
                perioada_vacanta=locatii_tara.get(i).getPerioadaVacanta();
                boolean sejur_posibil=false;
                //verific locatiile in care pot sa merg in vacanta in intervalul respectiv (tot intervalul,ca sa nu ma dea afara dupa doua zile)
                if (an_start>=perioada_vacanta.an_inceput&&luna_start>=perioada_vacanta.luna_inceput&&zi_start>=perioada_vacanta.zi_inceput&&an_final<=perioada_vacanta.an_final&&luna_final<=perioada_vacanta.luna_final&&zi_final<=perioada_vacanta.zi_final)
                {
                    sejur_posibil=true;
                }
                if (sejur_posibil)
                {
                    Locatie locatie;
                    locatie=locatii_tara.get(i);
                    int nr_zile=(int)((an_final-an_start)*365+((luna_final-luna_start)*365)/12+(zi_final-zi_start));
                    locatie.cost_total=nr_zile*locatie.getPretMediuPeZi();
                    locatii_tara_sortate.add(locatie);
                }
            }
            if (locatii_tara_sortate.size()>0)
            {
                Collections.sort(locatii_tara_sortate, new ComparatorPret());
                //afisare top5
                LocationData location_info=new LocationData("Top 5 locatii din "+tara);
                String informatii="";
                if (locatii_tara_sortate.size()>=5)
                {
                    for (int i=0;i<5;i++)
                    {
                        informatii=informatii+Integer.toString(i+1)+"."+locatii_tara_sortate.get(i).nume_locatie+" -cost sejur "+Integer.toString((int) (locatii_tara_sortate.get(i).cost_total/locatii_tara_sortate.get(i).getPretMediuPeZi()))+" zile: "+Integer.toString((int) locatii_tara_sortate.get(i).cost_total)+" "+locatii_tara_sortate.get(i).getMonedaNationala()+"\n";               
                    }
                    location_info.date_locatie_text_area.setText(informatii);
                }
                else
                {
                    for (int i=0;i<locatii_tara_sortate.size();i++)
                    {
                        informatii=informatii+Integer.toString(i+1)+"."+locatii_tara_sortate.get(i).nume_locatie+" -cost sejur "+Integer.toString((int) (locatii_tara_sortate.get(i).cost_total/locatii_tara_sortate.get(i).getPretMediuPeZi()))+" zile: "+Integer.toString((int) locatii_tara_sortate.get(i).cost_total)+" "+locatii_tara_sortate.get(i).getMonedaNationala()+"\n";               
                    }   
                    location_info.date_locatie_text_area.setText(informatii);
                }
            }
            else
            {
                top_five_feedback_label.setText("Nu sunt locatii disponibile in aces interval");
            }
            //sterge continutul casutelor daca e nevoie(mie mi se pare mai productiv pentru utilizator sa nu le sterg)
        }
        else
        {
            top_five_feedback_label.setText("Locatii indisponibile in aceasta tara");
            //sterge continutul casutelor daca e nevoie(mie mi se pare mai productiv pentru utilizator sa nu le sterg)
        }
    }
    
    
    
    
    public void displayTopFiveInJudet(String judet,int zi_start,int luna_start,int an_start,int zi_final,int luna_final,int an_final)
    {
        locatii_judet_sortate=new ArrayList<>();
        ArrayList<Locatie> locatii_judet;
        if (Main.locatii_judete.containsKey(judet))
        {
            locatii_judet=Main.locatii_judete.get(judet);
            for (int i=0;i<locatii_judet.size();i++)
            {
                PerioadaVacanta perioada_vacanta;
                perioada_vacanta=locatii_judet.get(i).getPerioadaVacanta();
                boolean sejur_posibil=false;
                //verific locatiile in care pot sa merg in vacanta in intervalul respectiv (tot intervalul,ca sa nu ma dea afara dupa doua zile)
                if (an_start>=perioada_vacanta.an_inceput&&luna_start>=perioada_vacanta.luna_inceput&&zi_start>=perioada_vacanta.zi_inceput&&an_final<=perioada_vacanta.an_final&&luna_final<=perioada_vacanta.luna_final&&zi_final<=perioada_vacanta.zi_final)
                {
                    sejur_posibil=true;
                }
                if (sejur_posibil)
                {
                    Locatie locatie;
                    locatie=locatii_judet.get(i);
                    int nr_zile=(int)((an_final-an_start)*365+((luna_final-luna_start)*365)/12+(zi_final-zi_start));
                    locatie.cost_total=nr_zile*locatie.getPretMediuPeZi();
                    locatii_judet_sortate.add(locatie);
                }
            }
            if(locatii_judet_sortate.size()>0)
            {    
                Collections.sort(locatii_judet_sortate, new ComparatorPret());
                //afisare top5
                LocationData location_info=new LocationData("Top 5 locatii din judetul "+judet);
                String informatii="";
                if (locatii_judet_sortate.size()>=5)
                {
                    for (int i=0;i<5;i++)
                    {
                        informatii=informatii+Integer.toString(i+1)+"."+locatii_judet_sortate.get(i).nume_locatie+" -cost sejur "+Integer.toString((int) (locatii_judet_sortate.get(i).cost_total/locatii_judet_sortate.get(i).getPretMediuPeZi()))+" zile: "+Integer.toString((int) locatii_judet_sortate.get(i).cost_total)+" "+locatii_judet_sortate.get(i).getMonedaNationala()+"\n";               
                    }
                    location_info.date_locatie_text_area.setText(informatii);
                }
                else
                {
                    for (int i=0;i<locatii_judet_sortate.size();i++)
                    {
                        informatii=informatii+Integer.toString(i+1)+"."+locatii_judet_sortate.get(i).nume_locatie+" -cost sejur "+Integer.toString((int) (locatii_judet_sortate.get(i).cost_total/locatii_judet_sortate.get(i).getPretMediuPeZi()))+" zile: "+Integer.toString((int) locatii_judet_sortate.get(i).cost_total)+" "+locatii_judet_sortate.get(i).getMonedaNationala()+"\n";               
                    }
                    location_info.date_locatie_text_area.setText(informatii);
                }
            }
            else
            {
                top_five_feedback_label.setText("Nu sunt locatii disponibile in aces interval");
            }
            //sterge continutul casutelor daca e nevoie(mie mi se pare mai productiv pentru utilizator sa nu le sterg)
        }
        else
        {
            top_five_feedback_label.setText("Locatii indisponibile in acest judet");
           //sterge continutul casutelor daca e nevoie(mie mi se pare mai productiv pentru utilizator sa nu le sterg)
        }
    }
    
    
    
    public void displayTopFiveInOras(String oras,int zi_start,int luna_start,int an_start,int zi_final,int luna_final,int an_final)
    {
        locatii_oras_sortate=new ArrayList<>();
        ArrayList<Locatie> locatii_oras;
        if (Main.locatii_orase.containsKey(oras))//daca exista orasul in hashmap
        {
            locatii_oras=Main.locatii_orase.get(oras);
            for (int i=0;i<locatii_oras.size();i++)
            {
                PerioadaVacanta perioada_vacanta;
                perioada_vacanta=locatii_oras.get(i).getPerioadaVacanta();
                boolean sejur_posibil=false;
                //verific locatiile in care pot sa merg in vacanta in intervalul respectiv (tot intervalul,ca sa nu ma dea afara dupa doua zile)
                if (an_start>=perioada_vacanta.an_inceput&&luna_start>=perioada_vacanta.luna_inceput&&zi_start>=perioada_vacanta.zi_inceput&&an_final<=perioada_vacanta.an_final&&luna_final<=perioada_vacanta.luna_final&&zi_final<=perioada_vacanta.zi_final)
                {
                    sejur_posibil=true;
                }
                if (sejur_posibil)
                {
                    Locatie locatie;
                    locatie=locatii_oras.get(i);
                    int nr_zile=(int)((an_final-an_start)*365+((luna_final-luna_start)*365)/12+(zi_final-zi_start));
                    locatie.cost_total=nr_zile*locatie.getPretMediuPeZi();
                    locatii_oras_sortate.add(locatie);
                }
            }
            
            if (locatii_oras_sortate.size()>0)
            {
                Collections.sort(locatii_oras_sortate, new ComparatorPret());
                //afisare top5
                LocationData location_info=new LocationData("Top 5 locatii din orasul "+oras);
                String informatii="";
                if (locatii_oras_sortate.size()>=5)
                {
                    for (int i=0;i<5;i++)
                    {
                        informatii=informatii+Integer.toString(i+1)+"."+locatii_oras_sortate.get(i).nume_locatie+" -cost sejur "+Integer.toString((int) (locatii_oras_sortate.get(i).cost_total/locatii_oras_sortate.get(i).getPretMediuPeZi()))+" zile: "+Integer.toString((int) locatii_oras_sortate.get(i).cost_total)+" "+locatii_oras_sortate.get(i).getMonedaNationala()+"\n";               
                    }
                    location_info.date_locatie_text_area.setText(informatii);
                }
                else
                {
                    for (int i=0;i<locatii_oras_sortate.size();i++)
                    {
                        informatii=informatii+Integer.toString(i+1)+"."+locatii_oras_sortate.get(i).nume_locatie+" -cost sejur "+Integer.toString((int) (locatii_oras_sortate.get(i).cost_total/locatii_oras_sortate.get(i).getPretMediuPeZi()))+" zile: "+Integer.toString((int) locatii_oras_sortate.get(i).cost_total)+" "+locatii_oras_sortate.get(i).getMonedaNationala()+"\n";               
                    }
                    location_info.date_locatie_text_area.setText(informatii);
                }
            }
            else
            {
                top_five_feedback_label.setText("Nu sunt locatii disponibile in aces interval");
            }
           //sterge continutul casutelor daca e nevoie(mie mi se pare mai productiv pentru utilizator sa nu le sterg)
        }
        else
        {
            top_five_feedback_label.setText("Locatii indisponibile in acest oras");
           //sterge continutul casutelor daca e nevoie(mie mi se pare mai productiv pentru utilizator sa nu le sterg)
        }
    }
    
    
    public void DisplayActivitateIeftina(String activitate)
    {
        locatii_activitati_realizabile=new ArrayList<>();
        if (Main.locatii_activitati.containsKey(activitate))
        {
            ArrayList<Locatie> locatii_activitati=Main.locatii_activitati.get(activitate);//am facut rost de toate locatiile unde se poate practica activitatea   
            for (int i=0;i<locatii_activitati.size();i++)
                if (locatii_activitati.get(i).getPerioadaVacanta().nr_zile>=10)//verific daca pot sa practic aceasta activitate 10 zile
                {
                    locatii_activitati_realizabile.add(locatii_activitati.get(i));
                }
            if (locatii_activitati_realizabile.size()>0)
            {
            
                float min_pret=999999;
                for(int i=0;i<locatii_activitati_realizabile.size();i++)
                    if (locatii_activitati_realizabile.get(i).getPretMediuPeZi()<min_pret)//gasesc locatia cu cel mai mic pret pe zi
                    {
                        min_pret=locatii_activitati_realizabile.get(i).getPretMediuPeZi();
                        locatie_optima=locatii_activitati_realizabile.get(i);
                    }
                LocationData afisare_locatie_optima=new LocationData("Locatie optima");
                String informatii;
                informatii="Cea mai ieftina locatie in care se poate practica activitatea -"+activitate+"- :"+"\n\n"+locatie_optima.nume_locatie+" -costul practicarii activitatii timp de 10 zile: "+Integer.toString((int) (locatie_optima.getPretMediuPeZi()*10))+" "+locatie_optima.getMonedaNationala()+"\n";
                afisare_locatie_optima.date_locatie_text_area.setText(informatii);
                activitate_ieftina_feedback_label.setText("");
            }
            else
            {
                activitate_ieftina_feedback_label.setText("nu exista locatii disponibile");
                //nu exista locatii in care se poate practica activitatea timp de 10 zile
            }
        }
        else
        {
            activitate_ieftina_feedback_label.setText("nu exista locatii disponibile");
            //nu exista locatii in care sa se practice aceasta activitate
        }
    }
    
    }
